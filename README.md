# README #

PokeApp

### What is this repository for? ###

* Este repositório contem o código fonte do aplicativo PokeApp que exibe os Pokemons (originais) da API PokeApi (https://pokeapi.co)
* versõo 1.0

### How do I get set up? ###

* Fazer o clone da branch master

* Instalar o gerenciador de dependências CocoaPods ref https://cocoapods.org

* No diretório local onde foi feito o clone do projeto, executar a linha de comando abaixo para baixar as dependências
	 - pod install 

* Abrir no Xcode o arquivo do projeto PokeApp.xcworkspace
* Executar o Run do projeto no Xcode
