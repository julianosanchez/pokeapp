//
//  PokemonItemCollectionViewCell.swift
//  PokeApp
//
//  Created by Juliano Sanchez on 15/05/18.
//  Copyright © 2018 jsanchez. All rights reserved.
//

import UIKit
import Kingfisher

class PokemonItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var pokemonNameLabel: UILabel!
    @IBOutlet weak var favoriteImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 8.0
        self.clipsToBounds = true
    }

    func loadData(data: PokemonItem) {
        
        self.pokemonNameLabel.text = data.name
        
        if let pokemon = data.pokemon {
            self.pokemonImageView.kf.setImage(with: URL(string: pokemon.urlImage!)!)
            
            self.favoriteImageView.isHidden = !FavoritesManager.sharedInstance.isFavorite(pokemon: pokemon)
        }
    }
    
    override func prepareForReuse() {

        self.pokemonImageView.image = nil
        self.favoriteImageView.isHidden = true
    }
}
