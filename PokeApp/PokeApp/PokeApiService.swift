//
//  PokeApiService.swift
//  PokeApp
//
//  Created by Juliano Sanchez on 14/05/18.
//  Copyright © 2018 jsanchez. All rights reserved.
//

import Alamofire
import SwiftyJSON

class PokeApiService: NSObject {

    static let instance = PokeApiService()
    
    private var alamoFireManager : Alamofire.SessionManager!
    private let timeout : Double = 120
    private let baseURL: String = "http://pokeapi.co/api/v2/pokemon/"
    
    override init() {
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = TimeInterval(self.timeout)
        configuration.timeoutIntervalForResource = TimeInterval(self.timeout)
        
        alamoFireManager = Alamofire.SessionManager(configuration: configuration,
                                                    delegate: Alamofire.SessionDelegate(),
                                                    serverTrustPolicyManager: nil)
    }
    
    func getOriginalsPokemon(onCompletionHandler : @escaping (_ pokemons : [PokemonItem]?, _ error: String?) -> ()) {
        
        let url = self.baseURL + "?limit=150"
        
        alamoFireManager.request(url, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    var objList: [PokemonItem] = []
                    for (_, subJson) in json["results"] {
                        
                        let obj = PokemonItem(json: subJson)
                        objList.append(obj)
                    }
                    
                    onCompletionHandler(objList, nil)
                    
                } else {
                    onCompletionHandler(nil, nil)
                }
            case .failure:
                onCompletionHandler(nil, "Erro ao consultar Pokemons")
            }
        }
    }
    
    func getPokemon(id: Int, onCompletionHandler : @escaping (_ pokemon : Pokemon?, _ error: String?) -> ()) {
        
        let url = self.baseURL + String(id)
        
        alamoFireManager.request(url, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                
                if let value = response.result.value {
                    
                    let obj = Pokemon(json: JSON(value))
                    onCompletionHandler(obj, nil)
                    
                } else {
                    onCompletionHandler(nil, nil)
                }
            case .failure:
                onCompletionHandler(nil, "Erro ao consultar Pokemon")
            }
        }
    }
}
