//
//  FavoritesManager.swift
//  PokeApp
//
//  Created by Juliano Sanchez on 19/05/18.
//  Copyright © 2018 jsanchez. All rights reserved.
//

import Foundation

class FavoritesManager: NSObject {

    static let sharedInstance = FavoritesManager()
    
    private let key = "favoritesKey"
    private var favorites: [Int] = []
    
    override init() {
        
        let defaults = UserDefaults.standard
        self.favorites = defaults.array(forKey: self.key)  as? [Int] ?? [Int]()
    }
    
    func add(pokemon: Pokemon) {
        
        self.favorites.append(pokemon.id)
        self.saveFavorites()
    }
    
    func remove(pokemon: Pokemon) {
        
        if let index = self.favorites.index(of: pokemon.id) {
            self.favorites.remove(at: index)
            self.saveFavorites()
        }
    }
    
    func isFavorite(pokemon: Pokemon) -> Bool {
        
        guard self.favorites.index(of: pokemon.id) != nil else {
            return false
        }
        
        return true
    }
    
    func getAll() -> [Int] {
        
        return self.favorites
    }
    
    fileprivate func saveFavorites() {
        let defaults = UserDefaults.standard
        defaults.set(self.favorites, forKey: self.key)
    }
}
