//
//  ViewController.swift
//  PokeApp
//
//  Created by Juliano Sanchez on 15/05/18.
//  Copyright © 2018 jsanchez. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var pokemonItens: [PokemonItem] = []
    private var pokemons: [Pokemon] = []
    
    private let pokemonCellID = "pokeCell"
    private let pokemonDetailsSegue = "detailsSegue"
    private var cellSelectedIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupCollectionView()
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let index = self.cellSelectedIndex {
            
            let indexPath = IndexPath(item: index, section: 0)
            if let cellToUpdate = self.collectionView.cellForItem(at: indexPath) as? PokemonItemCollectionViewCell {

                let pokemonItem = self.pokemonItens[indexPath.row]
                cellToUpdate.loadData(data: pokemonItem)
                self.cellSelectedIndex = nil
            }
        }
    }

    fileprivate func loadData() {
    
        PokeApiService.instance.getOriginalsPokemon { [weak self] (pokemonsList, error) in
            
            if let pokemons = pokemonsList {
                self?.pokemonItens = pokemons
                self?.collectionView.reloadData()
            }
        }
    }
    
    fileprivate func showPokemonDetailsView(pokemon: Pokemon) {
        
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
        detailsVC.pokemon = pokemon
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    
}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func setupCollectionView() {
        
        // set delegate/datasource
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // register cell table view
        let cellNIB = UINib(nibName: "PokemonItemCollectionViewCell", bundle: nil)
        collectionView.register(cellNIB, forCellWithReuseIdentifier: self.pokemonCellID)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.pokemonItens.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let pokemonItem = self.pokemonItens[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.pokemonCellID, for: indexPath) as! PokemonItemCollectionViewCell
        
        cell.loadData(data: pokemonItem)

        // get pokemon data
        if pokemonItem.pokemon == nil {

            let pokemonId = indexPath.row + 1
            PokeApiService.instance.getPokemon(id: pokemonId) { [weak self] (pokemon, error) in

                guard let view = self else {return}
                
                // update cell
                view.pokemonItens[indexPath.row].pokemon = pokemon
                if let cellToUpdate = view.collectionView.cellForItem(at: indexPath) as? PokemonItemCollectionViewCell {
                    cellToUpdate.loadData(data: view.pokemonItens[indexPath.row])
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let pokemon = self.pokemonItens[indexPath.row].pokemon else { return }

        self.cellSelectedIndex = indexPath.row
        self.showPokemonDetailsView(pokemon: pokemon)
    }
}

