//
//  DetailsViewController.swift
//  PokeApp
//
//  Created by Juliano Sanchez on 18/05/18.
//  Copyright © 2018 jsanchez. All rights reserved.
//

import UIKit
import Kingfisher

class DetailsViewController: UIViewController {

    private struct ROWDEF {
        
        static let NAME: Int = 0
        static let TYPE: Int = 1
        static let HEIGHT: Int = 2
        static let WEIGHT: Int = 3
    }
    
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var pokemon : Pokemon?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
        self.setupFavoriteButton()
        self.loadData()
    }
    
    fileprivate func loadData() {
        
        guard let pokemon = self.pokemon else {return}
        
        if let urlImage = pokemon.urlImage,
            let resourceImage = URL(string: urlImage) {
            
            self.pokemonImageView.kf.setImage(with: resourceImage,
                                              placeholder: UIImage(),
                                              options: nil,
                                              progressBlock: nil,
                                              completionHandler: nil)
        }
        
        if FavoritesManager.sharedInstance.isFavorite(pokemon: pokemon) {
            self.favoriteButton.isSelected = true
        }
        
        self.navigationItem.title = pokemon.name
    }
    
    fileprivate func setupFavoriteButton() {
        
        self.favoriteButton.setImage(UIImage(named: "unfavorite"), for: .normal)
        self.favoriteButton.setImage(UIImage(named: "favorite"), for: .selected)
    }
    
    @IBAction func favoriteTouchUpInside(_ sender: UIButton) {
        
        if (sender.isSelected) {
            FavoritesManager.sharedInstance.remove(pokemon: self.pokemon!)
        } else {
            FavoritesManager.sharedInstance.add(pokemon: self.pokemon!)
        }
        
        sender.isSelected = !sender.isSelected
    }
}

extension DetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
    
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.tableFooterView = UIView()
    }
    
    func textLabelForRow(index: Int) -> String {
        switch index {
        case ROWDEF.NAME:
            return "Nome"
        case ROWDEF.TYPE:
            return "Tipo"
        case ROWDEF.HEIGHT:
            return "Altura"
        case ROWDEF.WEIGHT:
            return "Peso"
        default:
            return ""
        }
    }
    
    func detailLabelForRow(index: Int, pokemon: Pokemon?) -> String? {
        
        switch index {
        case ROWDEF.NAME:
            return pokemon?.name
            
        case ROWDEF.TYPE:
            return pokemon?.type
            
        case ROWDEF.HEIGHT:
            if let height = pokemon?.height {
                return "\(height)"
            } else {
                return nil
            }
            
        case ROWDEF.WEIGHT:
            if let weight = pokemon?.weight {
                return "\(weight)"
            } else {
                return nil
            }
            
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        
        cell.textLabel?.text = textLabelForRow(index: indexPath.row)
        cell.detailTextLabel?.text = detailLabelForRow(index: indexPath.row, pokemon: self.pokemon)
        
        return cell
    }
}

