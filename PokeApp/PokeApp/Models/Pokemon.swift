//
//  Pokemon.swift
//  PokeApp
//
//  Created by Juliano Sanchez on 15/05/18.
//  Copyright © 2018 jsanchez. All rights reserved.
//

import SwiftyJSON

struct Pokemon {
    
    var id: Int
    var name: String
    var urlImage: String?
    var height: Int?
    var weight: Int?
    var type: String?
    
    init (json: JSON) {
        
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.height = json["height"].int
        self.weight = json["weight"].int
        
        if let front_default = json["sprites"]["front_default"].string {
            
            self.urlImage = front_default
        } else {
            self.urlImage = nil
        }
        
        if json["types"] != JSON.null {
            
            if let typeName =  json["types"][0]["type"]["name"].string {
             
                self.type = typeName
            }
        }
    }
}
