//
//  Pokemon.swift
//  PokeApp
//
//  Created by Juliano Sanchez on 15/05/18.
//  Copyright © 2018 jsanchez. All rights reserved.
//

import SwiftyJSON

struct PokemonItem {
    
    var name: String
    var url: String?
    
    var pokemon: Pokemon?
    
    init (json: JSON) {
        
        self.name = json["name"].stringValue
        self.url = json["url"].stringValue
    }
}
